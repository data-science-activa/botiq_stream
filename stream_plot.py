import random
from itertools import count
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

plt.style.use("fivethirtyeight")

x_vals = []
y_vals = []

index = count()


def animate(i):
    df = pd.read_csv("stream.csv")
    x = df["Date"]
    y1 = df["Open"]

    plt.cla()

    plt.plot(x, y1, label="real")

    plt.legend(loc="upper left")
    plt.tight_layout()


ani = FuncAnimation(plt.gcf(), animate, interval=2000)

plt.tight_layout()
plt.show()
