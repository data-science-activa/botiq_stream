import json
import boto3
import random
import datetime
import time
import pandas as pd

kinesis = boto3.client("kinesis")
df = pd.read_csv("./GOOG (6).csv")
i = 0

# def getReferrer():
#     data = {}
#     now = datetime.datetime.now()
#     str_now = now.isoformat()
#     data['Date'] = df.Date[i]
#     data['TICKER'] = 'GOOG'
#     price = df.Open[i]
#     data['Open'] = round(price, 2)
#     i+=1
#     return data

while True:

    data = {}
    now = datetime.datetime.now()
    str_now = now.isoformat()
    data["Date"] = df.Date[i]
    data["TICKER"] = "GOOG"
    price = df.Open[i]
    data["High"] = round(price, 2)

    data = json.dumps(data)
    print(data)
    kinesis.put_record(
        StreamName="KinesisStream", Data=data, PartitionKey="partitionkey"
    )

    i += 1
    time.sleep(0.5)
