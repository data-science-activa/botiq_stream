import csv
import random
import time
import requests
import json

fieldnames = ["Date", "High"]


url = "http://ec2-13-52-7-61.us-west-1.compute.amazonaws.com:80/time_series/deploy"


headers = {
    "Content-Type": "application/json",
    "jwt": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiZGVlcGFrLnlAYWN0aXZhLm9uZSIsInVzZXJfaWQiOiI1ZjBlYWI2YjU5ZjNjNDcyNjJjYTc4YTciLCJmdWxsX25hbWUiOiJEZWVwYWsiLCJyb2xlX2lkIjoiNWRmOWU0Y2Q5YmVkMzk0ZjNjY2RiZGVhIiwidGltZXN0YW1wIjoxNjAyMTQ2NzM1NTc0LCJpYXQiOjE2MDIxNDY3MzUsImV4cCI6MzIwNDIwMjQ5OSwiYXVkIjoiaHR0cDovL2xvY2FsaG9zdDozMDAwIiwiaXNzIjoibG9jYWxob3N0In0.md4zDGSlpCVBiejJBzXoA5qy__iYpnD8huJVF6bp_V8",
}
f = open("forecast.csv", "w")
f.truncate()
f.close()

with open("forecast.csv", "w") as csv_file:
    csv_writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
    csv_writer.writeheader()


i = 0
# {algo_1: "FBprophet", algo_2: "N_BEATS", algo_3: "DeepAR", algo_4: "lgbm", algo_5: "V_lstm"}
# High
# lag= 24

while True:

    payload = {
        "algo": "lgbm",
        "new_file_url": f"https://mybucketstream.s3.us-east-2.amazonaws.com/stream_{i}.csv",
        "analysis_param_pickle_url": "https://s3.us-east-2.amazonaws.com/activa.one/deepak_1594797001302/analysis/1601537195916/parameters.pickle",
        "trained_model_url": "https://s3.us-east-2.amazonaws.com/activa.one/deepak_1594797001302/training/1601537195916/trained_model.pickle",
    }

    response = requests.request("GET", url, headers=headers, data=json.dumps(payload))
    print(response)

    print(f"stream {i} begin")
    print(payload["new_file_url"])

    # pt times to plot infernce value

    with open("forecast.csv", "a") as csv_file:

        csv_writer = csv.DictWriter(csv_file, fieldnames=fieldnames)

        data = response.json()["data"]["forecast"]["High"]

        for key in data.keys():
            print(f"{key}, {data[key]}, {i}")

            info = {"Date": key, "High": data[key]}
            csv_writer.writerow(info)

    # lag=10 * freq=1 sec for another request
    i += 1
    time.sleep(10)
