import requests
import json

url = "http://ec2-13-52-7-61.us-west-1.compute.amazonaws.com:80/time_series/deploy"

payload = {
    "algo": "lgbm",
    "new_file_url": f"https://timeseriesdataset.s3.us-east-2.amazonaws.com/stream_{0}",
    "analysis_param_pickle_url": "https://s3.us-east-2.amazonaws.com/activa.one/deepak_1580131130309/analysis/1600424268685/parameters.pickle",
    "trained_model_url": "https://s3.us-east-2.amazonaws.com/activa.one/deepak_1580131130309/training/1600424268685/trained_model.pickle",
}


headers = {
    "Content-Type": "application/json",
    "jwt": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiZGVlcGFrLnlAYWN0aXZhLm9uZSIsInVzZXJfaWQiOiI1ZjBlYWI2YjU5ZjNjNDcyNjJjYTc4YTciLCJmdWxsX25hbWUiOiJEZWVwYWsiLCJyb2xlX2lkIjoiNWRmOWU0Y2Q5YmVkMzk0ZjNjY2RiZGVhIiwidGltZXN0YW1wIjoxNjAxNDY0OTA0NjUwLCJpYXQiOjE2MDE0NjQ5MDQsImV4cCI6MzE5Nzk4NDgzMSwiYXVkIjoiaHR0cDovL2xvY2FsaG9zdDozMDAwIiwiaXNzIjoibG9jYWxob3N0In0.KLwDDytVVcd8NtIP2dRCgamvzla39KDApgMvS5VjJ90",
}

response = requests.request("GET", url, headers=headers, data=json.dumps(payload))

print(response.text.encode("utf8"))


# for key in data.keys():
#     print(f"{key}, {data[key]}")

# print(response.json()["data"]["forecast"]["Open"].keys())
